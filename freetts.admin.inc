<?php
// $Id$

/**
 * @file
 * Contains the administrative functions of the freetts module.
 *
 * This file is included by the core freetts module, and includes the
 * settings form.
 *
 */

/**
 * Menu callback for the freetts settings form.
 *
 */
function freetts_server_settings() {
  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('FreeTTS Server Settings'),
  );
  $form['settings']['freetts_server_settings'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Source URL'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
  );


  $form['settings']['freetts_server_settings']['freetts_remote_url'] = array(
    '#type'          => 'textfield',
    '#title'         => t('FreeTTS server location'),
    '#default_value' => variable_get('freetts_remote_url', 0),
    '#description'   => t('please give url for the freetts location E.g htt;//www.somewhere.com/'),
  );
  $form['settings']['freetts_server_settings']['freetts_user_key'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Freetts user key'),
    '#default_value' => variable_get('freetts_user_key', 0),
    '#description'   => t('userkey is similar to a username for the freetts interface. This must be the same as the value given in the freetts_settings.php file on the freeTTS server'),
  );
  $form['settings']['freetts_server_settings']['freetts_user_ssid'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Freetts user ssid'),
    '#default_value' => variable_get('freetts_user_ssid', 0),
    '#description'   => t('userssid is similar to a password for the freetts interface. This must be the same as the value given in the freetts_settings.php file on the freeTTS server'),
  );

//   $form['settings']['freetts_local_settings'] = array(
//    '#type'        => 'fieldset',
//    '#title'       => t('Local freetts settings'),
//    '#collapsible' => TRUE,
//    '#collapsed'   => FALSE,
//  );

//   $form['settings']['freetts_local_settings']['freetts_jar_path'] = array(
//    '#type'          => 'textfield',
//    '#title'         => t('Freetts.jar location'),
//    '#default_value' => variable_get('freetts_jar_path', FREETTS_DEFAULT_FREETTS_JAR_PATH),
//    '#description'   => t('The location of freetts jar relative to site root.'),
//  );

  return system_settings_form($form);
}

function freetts_general_settings() {
    $form = array();

    $form['defaults'] = array(
        '#type' => 'fieldset',
        '#title' => t('General Settings'),
    );

    $form['defaults']['freetts_default_engine'] = array(
        '#type' => 'radios',
        '#title' => 'Default TTS Engine',
        '#default_value' => variable_get(freetts_default_engine, 0),
        '#options' => array(
            t('None, audio uploads only'),
            t('Espeak (local only)'),
            t('FreeTTS Local'),
            t('FreeTTS Server'),
        ),
        '#description' => 'Add advanced help here on how to setup stuff up. None will force use of uploaded audio',
    );

    $form['defaults']['freetts_path_to_lame'] = array(

        '#type' => 'textfield',
        '#title' => 'Path to lame',
        '#default_value' => variable_get('freetts_path_to_lame', ''),
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => FALSE,
        '#description' => t('Enter the full path/to/lame'),

    );

    $form['defaults']['freetts_node_types'] = array(
      '#type' => 'checkboxes',
      '#title' => 'Node types to include TTS',
      '#description' => 'Check the node types for which you would like to add audio',
      '#options' => node_get_types('names'),
      '#default_value' => variable_get('freetts_node_types', 'none'),
       );
 
  $form = system_settings_form($form);

  $form['#submit'][] = 'freetts_node_settings';


 return $form;

}



function freetts_espeak_settings(){
    $form = array();

    $form['defaults']['freetts_path_to_espeak'] = array(
        '#type' => 'textfield',
        '#title' => 'Path to eSpeak',
        '#default_value' => variable_get('freetts_path_to_espeak', ''),
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => TRUE,
        '#description' => t('Enter the full path to the espeak executable e.g. /usr/bin/espeak'),

    );

        $form['defaults']['freetts_path_to_espeak_library'] = array(
        '#type' => 'textfield',
        '#title' => 'Path to eSpeak library directory',
        '#default_value' => variable_get('freetts_path_espeak_library', ''),
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => FALSE,
        '#description' => 'The full path to the espeak library directory. <br/>'.
                          'Leave blank to use defaults'

    );
        
       $form['defaults']['freetts_default_espeak_voice'] = array(
        '#type' => 'textfield',
        '#title' => 'Default voice',
        '#default_value' => variable_get('freetts_default_espeak_voice', FREETTS_DEFAULT_ESPEAK_VOICE),
        '#size' => 10,
        '#maxlength' => 20,
        '#required' => FALSE,
        '#description' => 'Use "espeak --voices" on the command line for a complete list of available voices',
    );

       //possibly add a collapsed field that lists available voices





    return system_settings_form($form);
}

function freetts_local_settings(){

    $form = array();

     $form['defaults']['freetts_path_to_freetts_local'] = array(
        '#type' => 'textfield',
        '#title' => 'Path to freetts.jar',
        '#default_value' => variable_get('freetts_path_to_freetts_local', ''),
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => FALSE,
        '#description' => t('Enter the full/path/to/freetts.jar'),

    );

     $form['defaults']['freetts_default_local_voice'] = array(
        '#type' => 'textfield',
        '#title' => 'FreeTTS default voice',
        '#default_value' => variable_get('freetts_default_local_voice', FREETTS_DEFAULT_LOCAL_VOICE), //create constant for this
        '#size' => 10,
        '#maxlength' => 40,
        '#required' => TRUE,
        '#description' => t('Voice options can be extended by installing mbrola'),

    );
 
      $form['defaults']['freetts_path_to_mbrola_local'] = array(
        '#type' => 'textfield',
        '#title' => 'Path to mbrola directory',
        '#default_value' => variable_get('freetts_path_to_mbrola_local', ''),
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => FALSE,
        '#description' => t('If left blank, assumes mbrola not installed'),

    );


     return system_settings_form($form);

}

//@todo move about forms so general options come first followed by espeak followed by server

//mp3 settings??? Bitrate

//how to check install of these command line programs -> does php understand cli return messages -> how to check exists.