<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function freetts_node_settings() {
  //should do drupal get path around here i think

  module_load_include('inc', 'content', 'includes/content.crud');

//  $title_field_name = 'tts_mp3_title_';
//  $body_field_name = 'tts_mp3_body_';
  
    $body_field_name = FILEFIELD_BODY_PREFIX;
//    $title_field_name = FILEFIELD_TITLE_PREFIX;


  $node_types = variable_get('freetts_node_types', NULL);

  foreach ($node_types as $node_type => $node_has_field) {

//    $tts_title_field = array(
//        'type_name' => $node_type,
//        'field_name' => $title_field_name . $node_type,
//    );

    $tts_body_field = array(
        'type_name' => $node_type,
        'field_name' => $body_field_name . $node_type,
    );


    //if the title field exists we assume the body field also exists (NOTE: we're no longer using body)
    $field_exists = content_field_instance_read($tts_body_field);
    //if node is selected

    if ($node_has_field) {

      if (is_array($field_exists) && empty($field_exists)) {

        //create the array structure required to build a new cck field
//        $title_field = _freetts_mp3_field_array($tts_title_field);
        $body_field = _freetts_mp3_field_array($tts_body_field);

//        content_field_instance_create($title_field);
        content_field_instance_create($body_field);
      }
    } else {

      if (is_array($field_exists) && !empty($field_exists)) {

        content_field_instance_delete($tts_body_field['field_name'], $node_type);
//        content_field_instance_delete($tts_title_field['field_name'], $node_type);
        //check if this function returns a boolean, if so make d_s_m conditional
        //check for deletion content_field_instance_read($tts_body_field)
        drupal_set_message('removed tts fields from ' . $node_type);
      }
    }
  }
}

function _freetts_add_fields_to_node($node_types) {
 return NULL;

  //copy and paste structures array
  //add to node
  //by setting up programatically
  //lose options for max filesize and so on.... should perhaps instead
}

function _freetts_remove_fields_from_node($params = array()) {
return NULL;

}

function _freetts_mp3_field_array($params = array()) {
  return array(

      //check that files are made available to views...

      'field_name' => $params['field_name'],
      'type_name' => $params['type_name'],
      'display_settings' =>
      array(
          'weight' => FREETTS_TYPE_FIELDS_WEIGHT,
          'parent' => '',
          'label' =>
          array(
              'format' => 'hidden',
          ),
          'teaser' =>
          array(
              'format' => 'default',
              'exclude' => 1,
          ),
          'full' =>
          array(
              'format' => 'default',
              'exclude' => 1,
          ),
          5 =>
          array(
              'format' => 'default',
              'exclude' => 0,
          ),
          4 =>
          array(
              'format' => 'default',
              'exclude' => 0,
          ),
      ),
      'widget_active' => '1',
      'type' => 'filefield',
      'required' => '0',
      'multiple' => '0',
      'db_storage' => '1',
      'module' => 'filefield',
      'active' => '1',
      'locked' => '0',
      'columns' =>
      array(
          'fid' =>
          array(
              'type' => 'int',
              'not null' => false,
              'views' => true,
          ),
          'list' =>
          array(
              'type' => 'int',
              'size' => 'tiny',
              'not null' => false,
              'views' => true,
          ),
          'data' =>
          array(
              'type' => 'text',
              'serialize' => true,
              'views' => true,
          ),
      ),
      'list_field' => '0',
      'list_default' => '0',
      'description_field' => '0',
      'widget' =>
      array(
          'file_extensions' => 'mp3',
          'file_path' => FREETTS_DIRECTORY,
          'progress_indicator' => 'bar',
          'max_filesize_per_file' => '',
          'max_filesize_per_node' => '',
          'label' => 'mp3 recording',
          'weight' => FREETTS_TYPE_FIELDS_WEIGHT,
          'description' => 'Upload an mp3 recording of this content',
          'type' => 'filefield_widget',
          'module' => 'filefield',
      )
  );
}
